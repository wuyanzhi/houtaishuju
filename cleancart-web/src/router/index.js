import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login'
import Home from '@/components/home'

import StoreMap from '@/components/Map/storemap';


import ProductList from '@/components/Product/list';

Vue.use(Router)


export default new Router({
  routes: [
    { path: '/',name: 'Login',component: Login},
    { path: '/home',    name:'Home',   component: Home ,
    children: [
      { 
        path: '',
        name:'productlist',
        component: ProductList
      },
      { path:'/storemap',
        name: 'storemap',
        component: StoreMap
      }
    ]
  }
 
  ]
})
