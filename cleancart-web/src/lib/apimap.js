import Api from './api' 
const apiMap = {
    Login: {url: Api.domain+'/admin/auth/login', method: 'post'},
    CartList: {url: Api.domain+'/admin/cart/list', method: 'get'},
    MemberProduct: {url: Api.domain+'/admin/product/memberProduct', method: 'get'}
    
}

export default apiMap